package conso;

public class Aubgabe3b {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String Zero = " ";
		
		System.out.printf( "%-12s", "Fahrenheit");
		System.out.print("|");
		System.out.printf("%10s\n", "Celcius");
		System.out.print("------------------------\n");
		
		
		int FH1 = -20;
		double CL1 = -28.8889;
		System.out.printf("%d%9s|     %.2f\n", FH1, Zero, CL1);
		
		
		int FH2 = -10;
		double CL2 = -23.3333;
		System.out.printf("%d%9s|     %.2f\n", FH2, Zero, CL2);
		
		System.out.print("+");
		int FH3 = 0;
		double CL3 = -17.7778;
		System.out.printf("%d%10s|     %.2f\n", FH3, Zero, CL3);
		
		System.out.print("+");
		int FH4 = 20;
		double CL4 = -6.6667;
		System.out.printf("%d%9s|      %.2f\n", FH4, Zero, CL4);
		
		System.out.print("+");
		int FH5 = 30;
		double CL5 = -1.1111;
		System.out.printf("%d%9s|      %.2f\n", FH5, Zero, CL5);
	}

}
