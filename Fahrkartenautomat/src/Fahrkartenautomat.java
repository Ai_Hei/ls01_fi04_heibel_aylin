﻿import java.util.Scanner;

class Fahrkartenautomat {

	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag;
		double zuZahlenderBetrag;

		do {
			zuZahlenderBetrag = fahrkartenBestellungErfassen();
			eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			fahrkartenAusgeben();
			rückgeldAusgabe(eingezahlterGesamtbetrag, zuZahlenderBetrag);
			System.out.println(
					"\nVergessen Sie nicht, den Fahrschein\n vor Fahrtantritt entwerten zu lassen!\n Wir wünschen Ihnen eine gute Fahrt.\n");
			System.out.println("Neuer Kaufvorgang\n");

		} while (true);

	}

	public static double fahrkartenBestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);

		String[] fahrkartenName = new String[10]; 
		double[] fahrkartenPreis = new double[10]; 
		boolean falscheAuswahl = true; 
		boolean nichtBezahlen = true;
		double zuZahlenderBetrag = 0;
		int anfrageTickets = 0; 
		int ihreWahlTickets = 0;
		boolean anzahlStimmtNicht = true;
		double zuZahlenderGesamtbetrag = 0; 
		
		
		
		fahrkartenName[0] = "Einzelfahrschein Berlin AB";
		fahrkartenName[1] = "Einzelfahrschein Berlin BC";
		fahrkartenName[2] = "Einzelfahrschein Berlin ABC";
		fahrkartenName[3] = "Kurzstrecke";
		fahrkartenName[4] = "Tageskarte Berlin AB";
		fahrkartenName[5] =	"Tageskarte Berlin BC";
		fahrkartenName[6] = "Tageskarte Berlin ABC";
		fahrkartenName[7] = "Kleingruppen-Tageskarte Berlin AB";
		fahrkartenName[8] = "Kleingruppen-Tageskarte Berlin BC";
		fahrkartenName[9] = "Kleingruppen-Tageskarte Berlin ABC";
		fahrkartenPreis[0] = 2.90;
		fahrkartenPreis[1] = 3.30;
		fahrkartenPreis[2] = 3.60;
		fahrkartenPreis[3] = 1.90;
		fahrkartenPreis[4] = 8.60;
		fahrkartenPreis[5] = 9.00;
		fahrkartenPreis[6] = 9.60;
		fahrkartenPreis[7] = 23.50;
		fahrkartenPreis[8] = 24.30;
		fahrkartenPreis[9] = 24.90;
		
		while (nichtBezahlen) {
	        falscheAuswahl= true;
		
			System.out.println("Wählen Sie die Fahrkarte aus:");
			
			for(int i= 0; i < fahrkartenName.length; i++) {
				System.out.println(i+1 + ". " + fahrkartenName[i] + " " + fahrkartenPreis[i] + "0 Euro");
			}
			
			System.out.println("11. Möchten Sie bezahlen?");
			
			while (falscheAuswahl) { 
				ihreWahlTickets = tastatur.nextInt();
				
				if (ihreWahlTickets <= 10 && ihreWahlTickets >= 1) {
					zuZahlenderBetrag = fahrkartenPreis[ihreWahlTickets-1];
					falscheAuswahl = false;
				}
				
				else if(ihreWahlTickets == 11) { 
						falscheAuswahl = false;
						nichtBezahlen = false;
						anzahlStimmtNicht = false;
				}
			
				else { 
					anzahlStimmtNicht = true;
					System.out.print("Die Angabe ist Falsch! Bitte geben Sie ein Ticket ein oder Bezahlen!");
				}
			}
			
				while(anzahlStimmtNicht) {
					System.out.println("Anzahl der gewünschten Tickets:");
					anfrageTickets = tastatur.nextInt();
					
					if (anfrageTickets >= 11) {
						anzahlStimmtNicht = true;
						nichtBezahlen = false;
						System.out.println("Sie können nicht mehr als 10 Ticket kaufen!");
					}
					
					else if (anfrageTickets <= 0) {
						anzahlStimmtNicht = true;
						nichtBezahlen = false;
						System.out.println("Sie können nicht weniger als 1 Ticket kaufen!");
					}
					else {
						break;
					}
					
					
			}
			 zuZahlenderGesamtbetrag = zuZahlenderGesamtbetrag + (zuZahlenderBetrag * anfrageTickets);
			 zuZahlenderBetrag = 0;
			 anfrageTickets = 0;
			 System.out.printf("Zu zahlender Betrag: %.2f €\n", zuZahlenderGesamtbetrag);
		}
		
		return zuZahlenderBetrag;
	}
					
	
			
				
		/*	
			
			System.out.print("Anzahl der Tickets: ");
	    	anzahlTickets = tastatur.nextInt();
	
				if ((anzahlTickets >= 1 && anzahlTickets <= 10) == false) {
					anzahlTickets = 1;
					System.out.println("Ihre Angabe ist Falsch! Ihre Ticketanzahl wurde als 1 übernommen.");
				}
				else {
					anzahlFalsch = false;
					
				}
			}
		 
			if(nichtBezahlen == false) {
				
			}
			
		}
		return zuZahlenderBetrag;

	} 
	
	*/
	
	
	
	

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");

	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			double ausstehenderBetrag = zuZahlenderBetrag - eingezahlterGesamtbetrag;
			System.out.printf("Noch zu zahlen: %.2f €\n", ausstehenderBetrag);
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro); ");
			double eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}

		return eingezahlterGesamtbetrag;

	}

	public static void rückgeldAusgabe(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		double rückgabebetrag;

		rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (rückgabebetrag > 0.0) {
			System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rückgabebetrag -= 0.05;
			}
		}
	}

	/* fertig */
	
}


