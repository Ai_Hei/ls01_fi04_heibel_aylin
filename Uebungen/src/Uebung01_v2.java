
public class Uebung01_v2 {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
      double wert1 = 2.0;
      double wert2 = 4.0;
      double m;
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      m = berechneMittelwert(wert1, wert2);
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", wert1, wert2, m);
   }
   
   public static double berechneMittelwert(double x, double y) {
	   
	   double ergebnis = (x + y) / 2.0;
	   return ergebnis;
	   
   }
}
